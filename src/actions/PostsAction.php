<?php

class PostAction
{
  public function run()
  {
    $posts = $this->getPosts();

    $this->render('posts', ['posts' => $posts]);
  }

  private function getPosts()
  {
    /** @todo сейчас функция возвращает все активные Посты
     * Нужно добавить фильтрацию по ID автора или по ID тега
     */

    return Post::model()
      ->active()
      ->recent()
      ->findAll();
  }
}
