<?php

class PostController
{
  public function actions()
  {
    return array(
      'posts' => 'application.actions.PostsAction',
      'post' => 'application.actions.PostAction'
    );
  }
}
